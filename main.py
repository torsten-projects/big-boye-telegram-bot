from telegram import Update
import telegram
from telegram.chat import Chat
from telegram.ext import Updater, CommandHandler, CallbackContext, MessageHandler
from telegram.message import Message
from telegram.files.document import Document
from os import getenv
import io
from PIL import Image
import re

from telegram.ext.filters import Filters


username_resolution_selection = {}


def on_message(update: Update, context: CallbackContext) -> None:
	global username_resolution_selection
	message: Message = update.message
	if message is None:
		print(f'[ REJECT ] No message')
		return
	chat: Chat = message.chat
	username = 'Unknown'
	try:
		username = message.from_user.name
	except:
		pass
	# try:
	if message_content := message.text:
		message_content = message_content.strip()
		if re.match(r'^\d+$', message_content):
			target_side = int(message_content)
			if not (1 <= target_side <= 4096):
				print(f'[ REJECT ] Target resolution big ({target_side}x{target_side}) - {username}')
				chat.send_message('Target resolution big, keep it between 1 and 4096 inclusive')
				return
			username_resolution_selection[username] = target_side
			chat.send_message(f'Set default resolution to {target_side}x{target_side}')
			return

	document: Document = message.document
	if document is None:
		print(f'[ REJECT ] No document - {username}')
		chat.send_message('Send me an uncompressed picture')
		return
	mime_type = document.mime_type
	if mime_type != 'image/png':
		print(f'[ REJECT ] Invalid mime ({mime_type}) - {username}')
		chat.send_message('Send me an uncompressed PNG')
		return
	file: telegram.File = document.get_file()
	file_name = document.file_name
	file_size = file.file_size
	if file_size > 32 * 1024 * 1024:
		print(f'[ REJECT ] Too big ({file_size}) - {username}')
		chat.send_message('No files over 32 MB')
		return
	target_size = None
	if message_content := message.caption:
		message_content = message_content.strip()
		if re.match(r'^\d+$', message_content):
			target_side = int(message_content)
			if not (1 <= target_side <= 4096):
				print(f'[ REJECT ] Target resolution big ({target_side}x{target_side}) - {username}')
				chat.send_message('Target resolution big, keep it between 1 and 4096 inclusive')
				return
			target_size = (target_side, target_side)
	byte_array: bytearray = file.download_as_bytearray()
	with io.BytesIO(byte_array) as bytes_io:
		image = Image.open(bytes_io)
		initial_size = image.size
		filter = Image.LANCZOS
		if target_size is None:
			if username in username_resolution_selection:
				target_size = (username_resolution_selection[username], username_resolution_selection[username])
			else:
				target_size = (initial_size[0] * 10, initial_size[1] * 10)
				filter = Image.NEAREST
		new_image = image.resize(target_size, filter)
		with io.BytesIO() as bytes_io_2:
			new_image.save(bytes_io_2, format = 'PNG')
			bytes_io_2.seek(0)
			bytes_io_2.name = file_name
			chat.send_document(caption = f'Resized to {target_size}', document = bytes_io_2)
			print(
				f'[ ACCEPT ] Resized ({initial_size[0]} x {initial_size[1]} -> {target_size[0]} x {target_size[1]}) - {username}'
			)
	# except Exception as e:
	# 	print(f'[ REJECT ] Error ({e}) - {username}')


def main():
	telegram_api_key = getenv('TELEGRAM_BOT_API_KEY')
	updater = Updater(telegram_api_key, use_context = True)

	updater.dispatcher.add_handler(MessageHandler(Filters.update.message, on_message))

	updater.start_polling()
	print(f'[ READY  ] Ready to accept documents')
	updater.idle()


if __name__ == '__main__':
	main()
